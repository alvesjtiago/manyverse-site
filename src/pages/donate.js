import React from 'react'
import Layout from '../components/layout'
import SEO from '../components/seo'
import styles from './donate.module.css'

function* range(from, to) {
  for (let i = from; i <= to; i++) {
    yield i
  }
}

const Backers = ({amount}) => (
  <>
    {Array.from(range(0, amount - 1)).map(i => (
      <a
        href={`https://opencollective.com/manyverse/backer/${i}/website`}
        key={`backer${i}`}
        target="_blank"
        rel="noopener noreferrer"
      >
        <img
          src={`https://opencollective.com/manyverse/backer/${i}/avatar.svg`}
          alt="Avatar of a backer on OpenCollective"
        />
      </a>
    ))}
  </>
)

const Sponsors = ({amount}) => (
  <>
    {Array.from(range(0, amount - 1)).map(i => (
      <a
        href={`https://opencollective.com/manyverse/sponsor/${i}/website`}
        key={`sponsor${i}`}
        target="_blank"
        rel="noopener noreferrer"
      >
        <img
          src={`https://opencollective.com/manyverse/sponsor/${i}/avatar.svg`}
          alt="Avatar of a sponsor on OpenCollective"
        />
      </a>
    ))}
  </>
)

const DonatePage = () => (
  <>
    <Layout>
      <SEO title="Donate" />
      <div className={styles.opencollective}>
        <div className={styles.ocText}>
          <h1>Join our OpenCollective</h1>
          <p>
            Together with dozens of other backers, you can become a backer or a
            sponsor on our OpenCollective page. You will also join a{' '}
            <strong>monthly newsletter exclusively for backers</strong>.
          </p>
          <p>
            Everything is transparent. The funds are used by{' '}
            <a href="https://opencollective.com/manyverse#team">our team</a> to
            work primarily on app development, towards our{' '}
            <a href="https://gitlab.com/staltz/manyverse/wikis/roadmap">
              feature roadmap
            </a>
            . The funds are utilized according to our public{' '}
            <a href="https://gitlab.com/staltz/manyverse/wikis/financial-plan">
              financial plan
            </a>
            . The outcome of our work is seen on GitLab as{' '}
            <a href="https://gitlab.com/staltz/manyverse/commits">commits</a>{' '}
            and <a href="https://gitlab.com/staltz/manyverse/issues">issues</a>.
            You can also see the{' '}
            <a href="https://gitlab.com/staltz/manyverse/boards">issue board</a>{' '}
            to get a glance on work in progress and current priorities.
          </p>
          <a className={styles.btn} href="https://opencollective.com/manyverse">
            Become a backer
          </a>
        </div>
        <div style={{width: 80, height: 10}} />
        <div className={styles.ocBackers}>
          <h2>Backers</h2>
          <Backers amount={22} />
          <h2>Sponsors</h2>
          <Sponsors amount={2} />
        </div>
      </div>
      <div style={{textAlign: 'center', marginTop: 100, marginBottom: 80}}>
        <h1>Cryptocurrencies</h1>
        <p>
          Alternatively, you can also send us funds in any of the following
          cryptocurrencies:
        </p>
        <p>
          <a
            className={styles.btn}
            href="https://www.blockchain.com/btc/address/3NNGfHL96UrjggaBVQojF1mnGnXNx1SXv7"
            style={{marginRight: 20}}
          >
            Bitcoin
          </a>
          <a
            className={styles.btn}
            href="https://bitcoincash.blockexplorer.com/address/1PqqzgwmsU6kDTYhsr3y9E4DTM5LFFjBzZ"
            style={{marginRight: 20}}
          >
            Bitcoin Cash
          </a>
          <a
            className={styles.btn}
            href="https://etherscan.io/address/0x5E91C443f6Cc52025856d979ad7F0070955E882f"
            style={{marginRight: 20}}
          >
            Ethereum
          </a>
          <a
            className={styles.btn}
            href="https://zechain.net/account/t1g3umLApxXTXxXfe7pRGT5MnRQkrqntgyj"
            style={{marginRight: 20}}
          >
            Zcash
          </a>
        </p>
      </div>
    </Layout>
  </>
)

export default DonatePage
