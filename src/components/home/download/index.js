import React from 'react'
import {Link} from 'gatsby'
import googlePlayBadge from '../../../images/google-play-badge.png'
import fdroidBadge from '../../../images/fdroid-badge.png'
import logoSquare from '../../../images/logo-square.png'
import styles from './styles.module.css'

const DownloadSection = props => (
  <section className={styles.download}>
    <div className="content">
      <div
        style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}
      >
        <h1>Download</h1>

        <img
          src={logoSquare}
          alt="Manyverse app logo"
          className={styles.appLogo}
        />

        <a href="https://play.google.com/store/apps/details?id=se.manyver">
          <img src={googlePlayBadge} alt="Get it on Google Play" height="80" />
        </a>

        <a href="https://f-droid.org/app/se.manyver">
          <img src={fdroidBadge} alt="Get it on F-Droid" height="80" />
        </a>

        <div className={styles.iosBadge}>Not (yet) on iOS</div>
      </div>
    </div>
    <div className="content">
      <h1>Beta version</h1>
      <p>
        Manyverse already works, but it is still in beta. So far we have built:
      </p>
      <ul>
        <li>
          SSB (<a href="https://www.scuttlebutt.nz">Scuttlebutt</a>)
          communications
        </li>
        <li>Sync via Bluetooth, LAN, or internet</li>
        <li>Posts and comments</li>
        <li>Blocks and mutes</li>
        <li>Profiles</li>
        <li>Likes</li>
      </ul>
      <p>
        This is just the beginning. We have many more features planned in the{' '}
        <a href="https://gitlab.com/staltz/manyverse/wikis/roadmap">roadmap</a>,
        but we will need your help to get there.
      </p>
      <p>
        Read our <Link to="/blog">blog</Link> to keep up with updates to this
        project!
      </p>
    </div>
  </section>
)

export default DownloadSection
