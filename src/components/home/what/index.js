import React from 'react'
import styles from './styles.module.css'
import screenshot from '../../../images/screenshot2.png'

const WhatSection = props => (
  <section className={styles.whatSection}>
    <div className={styles.phoneShell}>
      <div className={styles.phoneHeader} />
      <img
        src={screenshot}
        width="100%"
        alt="Screenshot of the Manyverse app"
      />
      <div className={styles.phoneFooter} />
    </div>

    <div className={styles.whatText}>
      <h1>Familiar, yet radically new</h1>
      <p>
        Manyverse is a social network mobile app with features you would expect:
        posts, threads, likes, profiles, etc. But it's not running in the cloud
        owned by a company, instead, your friends' posts and all your social{' '}
        <strong>data live entirely in your phone</strong>. This way, even when
        you're offline, you can scroll, read anything, and even write posts and
        like content! When your phone is back online, it{' '}
        <strong>syncs the latest updates directly</strong> with your friends'
        phones, through a shared local Wi-Fi or on the internet.{' '}
      </p>
      <p>
        We're building this free and open source project as a community effort
        because we believe in non-commercial, neutral, and fair mobile
        communication for everyone.
      </p>
    </div>
  </section>
)

export default WhatSection
