import React from 'react'
import styles from './styles.module.css'
import googlePlayBadge from '../../../images/google-play-badge.png'
import fdroidBadge from '../../../images/fdroid-badge.png'

const CallToAction = props => (
  <section className={styles.cta}>
    <div className={styles.ctaBackground} />
    <a href="https://play.google.com/store/apps/details?id=se.manyver">
      <img src={googlePlayBadge} alt="Get it on Google Play" height="80" />
    </a>

    <a href="https://f-droid.org/app/se.manyver">
      <img src={fdroidBadge} alt="Get it on F-Droid" height="80" />
    </a>
  </section>
)

export default CallToAction
