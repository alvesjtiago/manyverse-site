import React from 'react'
import {graphql} from 'gatsby'
import Layout from '../components/layout'
import SEO from '../components/seo'
import styles from './blog.module.css'

export const pageQuery = graphql`
  query($path: String!) {
    markdownRemark(frontmatter: {path: {eq: $path}}) {
      html
      frontmatter {
        date(formatString: "YYYY-MM-DD")
        path
        author
        authorUrl
        title
      }
    }
  }
`

export default function Template({data}) {
  const {markdownRemark} = data
  const {frontmatter, html} = markdownRemark
  const {title, date, author, authorUrl} = frontmatter
  return (
    <Layout>
      <SEO title={title} />
      <div className={styles.blogContainer}>
        <h1>{title}</h1>
        <date className={styles.metadata}>{date}</date>
        {author && authorUrl ? (
          <>
            <br />
            <a href={authorUrl}>
              <span className={styles.metadata}>{author}</span>
            </a>
          </>
        ) : author ? (
          <>
            <br />
            <span className={styles.metadata}>{author}</span>
          </>
        ) : null}
        <div
          className={styles.blogContent}
          dangerouslySetInnerHTML={{__html: html}}
        />
      </div>
    </Layout>
  )
}
