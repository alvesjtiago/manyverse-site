const path = require('path')

exports.createPages = async ({actions, graphql}) => {
  const {createPage} = actions

  const blogPostTemplate = path.resolve(`src/templates/blog.js`)

  const result = await graphql(`
    {
      allMarkdownRemark(
        sort: {order: DESC, fields: [frontmatter___date]}
        limit: 1000
      ) {
        edges {
          node {
            frontmatter {
              path
            }
          }
        }
      }
    }
  `)

  if (result.errors) {
    return Promise.reject(result.errors)
  }

  for (let edge of result.data.allMarkdownRemark.edges) {
    createPage({
      path: edge.node.frontmatter.path,
      component: blogPostTemplate,
      context: {},
    })
  }
}
